﻿namespace WebSocket.Structures
{
    public class TickerResponse
    {
        public string stream { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string e { get; set; }
        public long E { get; set; }
        public string s { get; set; }
        public decimal c { get; set; }
        public decimal o { get; set; }
        public decimal h { get; set; }
        public decimal l { get; set; }
        public decimal v { get; set; }
        public decimal q { get; set; }
    }
}
