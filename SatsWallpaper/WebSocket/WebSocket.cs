﻿using System;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


public class CWebSocket
{
    private readonly ClientWebSocket Socket;

    public event EventHandler<string> OnMessage;

    public event EventHandler<Exception> OnError;

    public event EventHandler OnOpen;
    public WebSocketState State => Socket.State;

    public CWebSocket(EventHandler<string> OnMessage, EventHandler OnOpen = null, EventHandler<Exception> OnError = null) : this()
    {
        this.OnMessage += OnMessage;
        this.OnOpen += OnOpen;
        this.OnError += OnError;
    }
    public CWebSocket()
    {
        Socket = new ClientWebSocket();
    }

    public async Task Connect(string URL)
    {
        try
        {
            await Socket.ConnectAsync(new Uri(URL), new CancellationToken());
            while (Socket.State != WebSocketState.Open) ;

            Online = true;
            new Thread(Receive).Start();
            OnOpen.Invoke(this, null);
        }
        catch (Exception x) { OnError?.Invoke(this, x); }
    }
    internal async void Receive()
    {
        var MainBufferSize = 4024 * 4024 * 5;
        var buffer = new byte[MainBufferSize];
        int Count = 0;

        while (Online)
        {
            try
            {
                var Result = await Socket.ReceiveAsync(new ArraySegment<byte>(buffer, Count, MainBufferSize - Count), CancellationToken.None);

                if ((Count += Result.Count) == 0 || !Result.EndOfMessage)
                    continue;

                if (Online)
                    OnMessage?.Invoke(this, Encoding.UTF8.GetString(buffer, 0, Count));

                Count = 0;
            }
            catch (Exception x)
            {
                OnError?.Invoke(this, x);
                break;
            }
        }
    }

    public async void CloseAsync(WebSocketCloseStatus closeStatus, string statusDescription = "")
    {
        Online = false;
        try
        {
            await Socket.CloseAsync(closeStatus, statusDescription, new CancellationToken());
        }
        catch { }
    }

    public async Task SendAsync(string Msg)
    {
        try
        {
            await Socket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(Msg)), WebSocketMessageType.Text, true, new CancellationToken());
        }
        catch { }
    }

    public bool Online { get; internal set; } = false;
}