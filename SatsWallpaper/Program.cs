using Microsoft.Win32;
using System;
using System.Net.WebSockets;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;

namespace SatoshiWallpaper
{
    static class Program
    {
        static CWebSocket Socket = new();
        static decimal CurrentPrice { get; set; }

        static int UpdateInterval = 10000;

        [STAThread]
        static void Main()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);

            key.SetValue(@"WallpaperStyle", 0.ToString());
            key.SetValue(@"TileWallpaper", 0.ToString());

            while (true)
            {
                try
                {
                    if (Socket.State != WebSocketState.Open)
                        Restart();

                    CWallpaper.Set($"${CurrentPrice:0.0000000}");
                }
                catch { }
                Thread.Sleep(UpdateInterval);
            };
        }

        static void Restart()
        {
            try { Socket?.CloseAsync(WebSocketCloseStatus.NormalClosure); } catch { }

            Socket = new CWebSocket((sender, Data) =>
            {
                try
                {
                    var response = JsonSerializer.Deserialize<WebSocket.Structures.TickerResponse>(Data, new JsonSerializerOptions
                    {
                        NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString
                    });

                    if ((response ?? null)?.stream == "btcusdt@miniTicker")
                        CurrentPrice = (response.data?.c ?? 1) / 100000000;
                }
                catch { }
            },

            (sender, e) =>
            {
                Socket.SendAsync("{\"method\":\"SUBSCRIBE\",\"params\":[\"btcusdt@miniTicker\"],\"id\":1}").Wait();
            },

            (sender, e) =>
            {
                Restart();
            });

            Socket.Connect("wss://stream.binance.com:9443/stream").Wait();

        }
    }
}
