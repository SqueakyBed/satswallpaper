﻿using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

public static class CWallpaper
{
    const int SPI_SETDESKWALLPAPER = 20;
    const int SPIF_UPDATEINIFILE = 0x01;
    const int SPIF_SENDWININICHANGE = 0x02;

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

    public static Font defaultFont = new("Trebuchet MS", 65, FontStyle.Bold);

    public static void Set(string Text)
    {
        var textSize = Graphics.FromImage(new Bitmap(1, 1)).MeasureString(Text, defaultFont);

        using var bmp = new Bitmap((int)textSize.Width, (int)textSize.Height);
        using var graphics = Graphics.FromImage(bmp);

        graphics.Clear(Color.Black);
        graphics.DrawString(Text, defaultFont, new SolidBrush(Color.Gold), 0, 0);
        graphics.Save();

        string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.bmp");
        bmp.Save(tempPath, System.Drawing.Imaging.ImageFormat.Bmp);

        int ret = SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, tempPath, SPIF_SENDWININICHANGE );
    }
}
